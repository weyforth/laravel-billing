<?php
/**
 * Billing Interface.
 *
 * Defines a set of common billing functions used by any
 * billing classes that implement it
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Billing;

interface BillingInterface
{


    /**
     * Bill the specified user a specified amount.
     *
     * @param BillingUserInterface $user        The user to bill.
     * @param float                $amount      The amount to bill.
     * @param string               $description A description of the product billed.
     * @param string               $currency    The currency in which to bill the user. 
     *
     * @return true or Exception
     */
    public function bill(
        BillingUserInterface $user,
        $amount,
        $description,
        $currency = 'gbp'
    );


    /**
     * Refund the last successful charge.
     *
     * Returns true if successful, false otherwise.
     *
     * @return boolean
     */
    public function refundLastCharge();


    /**
     * Get the ID of the last successful charge.
     *
     * @return string or null
     */
    public function getLastChargeId();


}
