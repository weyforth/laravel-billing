<?php
/**
 * Billing Service Provider.
 *
 * Provides interface to operate using
 * multiple billing services inc. Stripe.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Billing;

use Illuminate\Support\ServiceProvider;

class BillingServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('weyforth/billing');
    }


    /**
     * Register the default interface to use for dependency injection.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        $app->bind(
            'Weyforth\Billing\BillingInterface',
            'Weyforth\Billing\StripeBilling'
        );
    }


}
