<?php
/**
 * User interface to use with Billing class.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Billing;

interface BillingUserInterface
{


    /**
     * Provide an array of metadata to pass to the billing function.
     *
     * @return array
     */
    public function meta();


}
