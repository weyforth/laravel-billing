<?php
/**
 * Stripe Billing Implementation.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Billing;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Stripe;
use Stripe_Charge;
use Exception;

class StripeBilling implements BillingInterface
{

    /**
     * The last charge object or null.
     *
     * @var $lastCharge
     */
    protected $lastCharge = null;


    /**
     * Set up the Stripe API.
     */
    public function __construct()
    {
        Stripe::setApiKey(Config::get('billing::stripe.key.secret'));
    }


    /**
     * {@inheritdoc}
     */
    public function bill(
        BillingUserInterface $user,
        $amount,
        $description,
        $currency = 'gbp'
    ) {
        try {
            if (!($token = Input::get('stripeToken'))) {
                return new Exception(trans('admin.checkout.error.stripe-token'));
            }

            if ($amount == 0) {
                return new Exception(trans('admin.checkout.error.no-products'));
            }

            $charge = Stripe_Charge::create(
                array(
                    'description' => $description,
                    'amount' => $amount,
                    'currency' => $currency,
                    'card' => $token,
                    'metadata' => $user->meta()
                )
            );

            $this->lastCharge = $charge;

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function refundLastCharge()
    {
        try {
            if ($this->lastCharge !== null) {
                $ch = Stripe_Charge::retrieve($this->lastCharge->id);

                if ($ch) {
                    $ch->refund();
                }

                return true;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function getLastChargeId()
    {
        return $this->lastCharge->id;
    }


}
